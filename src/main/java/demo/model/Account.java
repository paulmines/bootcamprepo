package demo.model;

/**
 * Created by paul on 9/19/15.
 */
public class Account {

    public Account(){}

    public Account(String name){
        this.name = name;
    }

    private String name;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
