package demo.controller;

import demo.dao.AccountDao;
import demo.model.Account;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by paul on 9/19/15.
 */


@RestController
public class AccountController {

    @Autowired
    private AccountDao accountDao;

    @RequestMapping("/accountss")
    public List<Account> getAccounts(){
        return accountDao.getAll();
    }

    @RequestMapping(value = "/accounts/{name}", method = RequestMethod.GET)
    public Account getName(@PathVariable String name){
        return accountDao.getAccount(name);
    }

    @RequestMapping(value = "/accounts", method = RequestMethod.POST)
    public List<Account> account(@RequestBody Account account){

        accountDao.addAccount(account);

        return accountDao.getAll();
    }

    @RequestMapping("/hello")
    public String getHello(){
        return "Hello World";
    }
}
