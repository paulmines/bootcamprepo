package demo.dao;

import demo.model.Account;
import org.springframework.stereotype.Repository;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by paul on 9/19/15.
 */
@Repository
public class AccountDao {

    private static List<Account> accounts = new ArrayList<Account>();

    static {

        accounts.add(new Account("hello"));
        accounts.add(new Account("paul"));
        accounts.add(new Account("mines"));
        accounts.add(new Account("HELLO ALDUB"));

    }

    public List<Account> getAll(){

        return accounts;
    }

    public void addAccount(Account account){
        accounts.add(account);
    }

    public Account getAccount(String name) {

        for (Account a : accounts) {
            if (a.getName().equalsIgnoreCase(name))
                return a;

        }
return new Account("null");
//        switch (name){
//            case "hello":
//                return accounts.get(0);
//            case "paul":
//                return accounts.get(1);
//            case "mines":
//                return accounts.get(2);
//            default:
//                return accounts.get(3);
//        }

    }

}
